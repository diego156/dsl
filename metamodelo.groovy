import jenkins.model.*

//Variables Globales (Estas deben crearse como parametros manualmente en el template que requerira el template para generar el pipeline)
def PROJECT_NAME = "${PROJECT_NAME}".trim()
def FOLDER = "${FOLDER}".trim()
def NAMESPACE = "${NAMESPACE}".trim()
def GITLAB_REPOSITORY = "${GITLAB_REPOSITORY}".trim()
def GITLAB_BRANCH = "${GITLAB_BRANCH}".trim()

// Variables internas del template (son variables que siempre seran las mismas) 
def TEMPLATE_NAME = "NombreDelTemplate"
def NEXUS_REPOSITORY = "http://nexus.fiinlab-tech.com:8081/repository/maven-snapshots/"
def NEXUS_CREDENTIALS= "admin:admin123"
def JOB_DESCRIPTION= "Job de proyecto \""+PROJECT_NAME+"\" creado automaticamente con " + TEMPLATE_NAME

//Creacion de folder
if (!Jenkins.instance.getItemByFullName(FOLDER)) {
  folder(FOLDER) {
  }
}

//Pipeline
pipelineJob("${FOLDER}/${PROJECT_NAME}") {
	description(JOB_DESCRIPTION)
	keepDependencies(false)
    parameters{
        //Insertar los parametros necesarios para crear el pipeline aqui
        //Inicio de parametros
        credentialsParam("GIT_CREDENTIAL_ID") {
            description("Id de credenciales para acceso al repositorio")
            defaultValue("jalvarez")
            type("com.cloudbees.plugins.credentials.impl.UsernamePasswordCredentialsImpl")
            required(true)
        }
        stringParam("NOMBRE","Manuel","")
        stringParam("SALUDO",", Buenos Dias","") 
        //fin de paramentros
    }
    definition{
    	cps{
    		script(
//Insertar codigo del pipeline aqui
//Inicio de codigo
"""node(){

    stage('Test') {
        sh "echo 'Hola \${NOMBRE}\${SALUDO}'"
        }      
}
    		"""
//Fin de codigo
            )
    	}
    }
}