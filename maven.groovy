import jenkins.model.*

//Variables Globales (Estas deben crearse como parametros manualmente en el template que requerira el template para generar el pipeline)
def PROJECT_NAME = "${PROJECT_NAME}".trim()
def FOLDER = "${FOLDER}".trim()
def NAMESPACE = "${NAMESPACE}".trim()
def GITLAB_REPOSITORY = "${GITLAB_REPOSITORY}".trim()
def GITLAB_BRANCH = "${GITLAB_BRANCH}".trim()
def SONARQUBEURL= "${SONARQUBEURL}".trim()
def JOB_DESCRIPTION= "Job de proyecto \""+PROJECT_NAME+"\" creado automaticamente"

//Creacion de folder
if (!Jenkins.instance.getItemByFullName(FOLDER)) {
  folder(FOLDER) {
  }
}

//Pipeline
pipelineJob("${FOLDER}/${PROJECT_NAME}") {
	description(JOB_DESCRIPTION)
	keepDependencies(false)
    parameters{
        credentialsParam("GIT_CREDENTIAL_ID") {
            description("Id de credenciales para acceso al repositorio")
            defaultValue("gitlab-cred")
            type("com.cloudbees.plugins.credentials.impl.UsernamePasswordCredentialsImpl")
            required(true)
        }
    }
    definition{
    	cps{
    		script(
//Insertar codigo del pipeline aqui
//Inicio de codigo
""" 
   podTemplate(containers: [
    containerTemplate(name: 'kubectl', image: 'gcr.io/cloud-builders/kubectl', ttyEnabled: true, command: 'cat'),
    containerTemplate(name: 'gcloud', image: 'gcr.io/cloud-builders/gcloud', ttyEnabled: true, command: 'cat'),
    containerTemplate(name: 'maven', image: 'gcr.io/cloud-builders/mvn', ttyEnabled: true, command: 'cat')
  ]) {
    node(POD_LABEL) {
        stage('Quality Revision') {
            container('maven') {
                git '${GITLAB_REPOSITORY}'
                sh 'mvn clean  sonar:sonar -Dsonar.host.url=${SONARQUBEURL} -Dsonar.projectKey=${PROJECT_NAME} -Dsonar.projectName=${PROJECT_NAME} -Dmaven.test.skip=true -Dsonar.java.binaries=target/ -X -Dsonar.login="admin" -Dsonar.password="NewPassword"'
            }
        }
        stage('Build and push image with Container Builder') {
            container('gcloud') {
                sh '''
                    gcloud auth activate-service-account jenkins-gce@devops-plataforma.iam.gserviceaccount.com  --key-file=devops-plataforma.json
                    gcloud builds submit --config cloudbuild.yaml
                '''
            }
        }
        stage('Despliegue') {
                container('kubectl') {
                    step([\$class: 'KubernetesEngineBuilder', 
                            projectId: "devops-plataforma",
                            clusterName: "cluster-1",
                            manifestPattern: 'backend.yaml',
                            zone: "us-central1-c",
                            credentialsId: 'devops-plataforma',
                            verifyDeployments: true])
                }
            }
    }
}
    		"""
//Fin de codigo
            )
    	}
    }
}
